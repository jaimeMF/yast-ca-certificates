# Copyright (C) 2022  Jaime Marquínez Ferrándiz

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

require "date"
require "yast"

module CaCertificates
    class Entry

        attr_reader :issuer, :end_date, :start_date, :email, :fingerprint, :alias, :subject, :hash

        OPENSSL_DUMP = "openssl x509 -inform DER"

        def initialize(crt_path)
            @end_date = DateTime.parse(get_crt_value(crt_path, "-enddate"))
            @start_date = DateTime.parse(get_crt_value(crt_path, "-startdate"))
            @issuer = get_crt_value(crt_path, "-issuer")
            @email = get_crt_value(crt_path, "-email")
            @fingerprint = get_crt_value(crt_path, "-fingerprint")
            @alias = get_crt_value(crt_path, "-alias")
            @subject = get_crt_value(crt_path, "-subject")
            @hash = get_crt_value(crt_path, "-hash")
        end

        def self.all()
            anchors_path = "/etc/pki/trust/anchors"
            path = Yast::Path.new(".target.dir")
            files = Yast::SCR.Read(path, anchors_path)
            files.map do |f|
                new(File.join(anchors_path, f).to_s)
            end
        end

        private

            def get_crt_value(crt_path, value)
                cmd = "#{OPENSSL_DUMP} -in #{crt_path} #{value}".strip
                path = Yast::Path.new(".target.bash_output")
                cmd_result = Yast::SCR.Execute(path, cmd)
                if cmd_result["exit"].zero?
                    cmd_result["stdout"].lines.first.split("=", 2)[1]
                else
                    raise "Calling openssl failed: #{cmd_result["stderr"]}"
                end
            end
    end
end