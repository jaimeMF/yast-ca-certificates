# Copyright (C) 2022  Jaime Marquínez Ferrándiz

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

require "yast"

require "ca_certificates/query_presenter"

Yast.import "UI"
Yast.import "Label"
Yast.import "Popup"

module CaCertificates
  class EntriesDialog
    include Yast::UIShortcuts
    include Yast::I18n
    include Yast::Logger
    include Yast::UIShortcuts

    def initialize
      textdomain "ca-certificates"

      @query = CaCertificates::QueryPresenter.new

      read_entries
    end

    # Displays the dialog
    def run
      return unless create_dialog
  
      begin
        return event_loop
      ensure
        close_dialog
      end
    end

    # Simple event loop
    def event_loop
      loop do
        input = Yast::UI.UserInput
        case input
        when :cancel
          # Break the loop
          break
        when :refresh
          # The user clicked the refresh button
          read_entries
          redraw_table
        else
          log.warn "Unexpected input #{input}"
        end
      end
    end

    # Draws the dialog
    def create_dialog
      Yast::UI.OpenDialog(
        Opt(:decorated, :defaultsize),
        VBox(
          # Header
          Heading(_("Certificates entries")),

          # Refresh
          Right(PushButton(Id(:refresh), _("Refresh"))),
          VSpacing(0.3),

          # Log entries
          table,
          VSpacing(0.3),

          # Quit button
          PushButton(Id(:cancel), Yast::Label.QuitButton)
        )
      )
    end

    def close_dialog
      Yast::UI.CloseDialog
    end

    # Table widget to display log entries
    def table
      headers = @query.columns.map {|c| c[:label] }

      Table(
        Id(:entries_table),
        Opt(:keepSorting),
        Header(*headers),
        table_items
      )
    end

    

    def table_items
      # Return the result as an array of Items
      entries_fields = @entries.map  do |entry|
        @query.columns.map {|c| entry.send(c[:method]) }
      end
      entries_fields.map {|fields| Item(*fields) }
    end

    def redraw_table
      Yast::UI.ChangeWidget(Id(:entries_table), :Items, table_items)
    end

    def read_entries
      @entries = @query.entries
    rescue => e
      log.warn e.message
      @entries = []
      Yast::Popup.Message(e.message)
    end
  end
end