# Copyright (C) 2022  Jaime Marquínez Ferrándiz

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

require "ca_certificates/entry"
require "delegate"

module CaCertificates
  class EntryPresenter < SimpleDelegator
    def initialize(entry)
      __setobj__(entry)
    end

    # Original entry
    def entry
      __getobj__
    end

    def formatted_end_date
      end_date.strftime()
    end

    def formatted_start_date
      start_date.strftime()
    end
  end
end