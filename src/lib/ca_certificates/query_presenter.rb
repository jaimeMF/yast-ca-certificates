# Copyright (C) 2022  Jaime Marquínez Ferrándiz

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

require "ca_certificates/entry"
require "ca_certificates/entry_presenter"
require "delegate"

module CaCertificates
    class QueryPresenter < SimpleDelegator
        include Yast::I18n

        def initialize()
            textdomain "ca-certificates"
            @entries_internal = Entry.all
        end

        def entries
            @entries_internal.map {|entry| EntryPresenter.new(entry)}
        end

        def columns
            [
                {label: _("Valid from"), method: :formatted_start_date},
                {label: _("Valid until"), method: :formatted_end_date},
                {label: _("Email"), method: :email},
                {label: _("Fingerprint"), method: :fingerprint},
                {label: _("Alias"), method: :alias},
                {label: _("Hash"), method: :hash},                
                {label: _("Issuer"), method: :issuer},
                {label: _("Subject"), method: :subject},
            ]
        end
    end
end