#
# spec file for package yast2-ca-certificates
#
# Copyright (c) 2022 SUSE LLC
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via https://bugs.opensuse.org/
#


Name:           yast2-ca-certificates
Version:        0.1
Release:        0
Summary:        YaST2 - Certificates manager
Group:          System/YaST
License:        GPL-2.0-or-later
URL:            https://codeberg.org/jaimeMF/yast-ca-certificates
Source:         %{name}-%{version}.tar.bz2
BuildArch:      noarch
BuildRequires:  update-desktop-files
BuildRequires:  yast2
BuildRequires:  yast2-devtools
BuildRequires:  yast2-ruby-bindings
#for install task
BuildRequires:  rubygem(%{rb_default_ruby_abi}:yast-rake)
# for tests
BuildRequires:  rubygem(%{rb_default_ruby_abi}:rspec)

Requires:       yast2
Requires:       yast2-ruby-bindings

%description
A YaST2 module to manage the installed certificates.

%prep
%setup -q

%build

%check
rake test:unit

%install
%yast_install
%yast_metainfo

%files
%license COPYING
%{yast_clientdir}
%{yast_desktopdir}
%{yast_libdir}
%{_datadir}/metainfo/org.codeberg.jaimeMF.yast.ca-certificates.metainfo.xml

%changelog
